import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute) {

  }

  isAuth: boolean = false;

  ngOnInit(): void {
    this.isAuth =  Boolean(localStorage.getItem('auth'));
    if (this.isAuth == false && location.pathname == "/contacts") {
      alert('Для посещения этой страницы требуется авторизация!');
      this.router.navigate(['/login']);
    } else if (this.isAuth == true && location.pathname == "/login") {
      this.router.navigate(['/contacts']);
    }

  }
  


}
