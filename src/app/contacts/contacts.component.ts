import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { FormsModule }   from '@angular/forms';


@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.less']
})
export class ContactsComponent implements OnInit {

  constructor() { }

  list:any = [];
  newName: string = '';
  newContact: string = '';
  query: string = '';


  ngOnInit(): void {
    if (localStorage.getItem('list')) {
      this.list = JSON.parse(localStorage.getItem('list') || '{}');
    }
  }
  add() {
    if (this.newName != "" || this.newContact != "") {
      this.list.push({name: this.newName, contact: this.newContact, edit: false})
      localStorage.setItem('list', JSON.stringify(this.list));
      this.newName = '';
      this.newContact = '';
    } else
    alert('Введение пустого значения недопустимо');
  }
  delete(i: any) {
    this.list.splice(i, 1);
    localStorage.setItem('list', JSON.stringify(this.list));
  }
  save(i: any) {
    this.list[i].edit = false;
    localStorage.setItem('list', JSON.stringify(this.list));
  }
  edit(i: any) {
    this.list[i].edit = true
  }

}
