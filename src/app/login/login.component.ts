import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from '../app.component';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute) {

  }

  ngOnInit(): void {
  }


  onSubmit() {
    this.username == 'admin' && this.password == 'admin' ? this.auth() : alert('Неверные данные для входа')
  }

  auth() {
    localStorage.setItem('auth', '1');
    this.router.navigate(['/contacts']);
  }

  username: string = '';
  password: string = '';

}
